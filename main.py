#!/usr/bin/env python3
"""
Produced a reusable UUID from user input seed.
"""

__author__ = "Lars Scheideler"
__version__ = "0.1.0"
__license__ = "Unlicenced"

import uuid
import random
import csv
import sys
from pathlib import Path

FILE_NAME='UUID.csv'
PATH='./'+FILE_NAME

def write_csv_file():
    my_file = Path(PATH)
    if not my_file.is_file():
        with open(FILE_NAME, 'w', newline='') as csv_file:
            fieldnames = ['seed', 'uuid']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

def main():
    # Main entry point of the application
    seed = input("Please Enter a Seed: ")
    rd = random.Random()
    rd.seed(seed)
    reproducible_uuid = uuid.UUID(int=rd.getrandbits(128))
    uuid_as_string = str(reproducible_uuid)
    print(f'UUID: {uuid_as_string}')
    write_csv_file()
    newRow = "%s,%s\n" % (seed, reproducible_uuid)
    with open(FILE_NAME, "r") as r_csv_file, open(FILE_NAME, "a") as a_csv_file:
        existingLines = [line for line in csv.reader(r_csv_file, delimiter=',')]
        data_row = [seed, uuid_as_string]
        if data_row not in existingLines:
            a_csv_file.write(newRow)
    # Close the file object
    r_csv_file.close()
    a_csv_file.close()

if __name__ == '__main__':
    # Execute when the module is not initialized from an import statement
    main()